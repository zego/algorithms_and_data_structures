# Algorithms and Data Structures

In this project I'm going to upload some common algorithms and data structures.
I wrote some helpful commands & extra info for you.

---

## Getting Started
The following structure is to show you how to navigate through this project:
```
exercises/
    excersiceName/
        index.js
        test.js
```
The file index.js is going to have a brief description of the exercise and the solution to the exercise. And as you can imagine, the test.js file is going to have the tests of the exercise.

You must install jest to run the tests. Use the following command to install jest:
```
sudo npm i -g jest
```

Once jest is installed, use the following command to run all tests:
```
jest
```

Or run one set of tests:
```
jest excersiceName/test.js --watch
```

If this command throws you an error use instead:
```
jest excersiceName --watch
```
