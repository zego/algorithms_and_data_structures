// --- Directions
// Write a function that accepts an integer N
// and returns a NxN spiral matrix.
// --- Examples
//   matrix(2)
//     [[1, 2],
//     [4, 3]]
//   matrix(3)
//    [[1, 2, 3],
//     [8, 9, 4],
//     [7, 6, 5]]
//  matrix(4)
//     [[1,  2,  3, 4],
//     [12, 13, 14, 5],
//     [11, 16, 15, 6],
//     [10,  9,  8, 7]]
//  matrix(5)
//    [[ 1,  2,  3,  4, 5],
//     [14, 15, 16, 17, 6],
//     [13, 20, 19, 18, 7],
//     [12, 11, 10,  9, 8]]

function spiralMatrix(n) {
    // Create empty array of arrays called 'results'
    const results = [];

    for (let i = 0; i < n; i++) {
        results.push([]);
    }

    // Create a counter variable, starting at 1
    let counter = 1;
    // Create help variables, such as: startColumn and startRow = 0. endColumn and endRow = n - 1
    let startColumn = 0;
    let endColumn = n - 1;
    let startRow = 0;
    let endRow = n - 1;

    // As long as (startColumn <= endColumn) and (startRow <= endRow)
    while (startColumn <= endColumn && startRow <= endRow) {
    // Responsible for top row
    for(let i = startColumn; i <= endColumn; i++) {
        results[startRow][i] = counter;
        counter++;
    }

    // Increment startRow
    startRow++;

    // Responsible for right column
    for(let i = startRow; i <= endRow; i++) {
        results[i][endColumn] = counter;
        counter++;
    }

    // Decrement endColumn
    endColumn--;

    // Responsible for bottom row
    for(let i = endColumn; i >= startColumn; i--) {
        results[endRow][i] = counter;
        counter++;
    }

    // Decrement endRow
    endRow--;

    // Responsible for left column
    for(let i = endRow; i >= startRow; i--) {
        results[i][startColumn] = counter;
        counter++;
    }

    // Increment startColumn
        startColumn++;
    }

    return results;
}

module.exports = spiralMatrix;
