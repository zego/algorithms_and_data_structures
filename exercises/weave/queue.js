// --- Directions
// Implement a 'peek' method in this Queue class.
// Peek should return the last element (the next
// one to be returned) from the queue *without*
// removing it.

class Queue {
    constructor() {
        this.data = new Array();
    }

    add(element) {
        this.data.unshift(element);
    }

    remove() {
        return this.data.pop();
    }

    peek() {
        const element = this.data.pop();
        this.data.push(element);
        return element;
        // return this.data[this.data.length - 1];
    }
}

module.exports = Queue;
