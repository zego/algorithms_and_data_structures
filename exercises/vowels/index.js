// --- Directions
// Write a function that returns the number of vowels
// used in a string.  Vowels are the characters 'a', 'e'
// 'i', 'o', and 'u'.
// --- Examples
//   vowels('Hi There!') --> 3
//   vowels('Why do you ask?') --> 4
//   vowels('Why?') --> 0


function vowels(str) {
    const stringArray = str.toLowerCase().split('');
    const vowels = ['a', 'e', 'i', 'o', 'u'];
    let numberOfVowels = 0;

    for (let character of stringArray) {
        if (vowels.includes(character)) {
            numberOfVowels++;
        }
    }

    return numberOfVowels;
}

module.exports = vowels;

// Solution 2
// function vowels(str) {
//     const matches = str.match(/[aeiou]/gi);
//     return martches ? matches.length : 0;
// }
