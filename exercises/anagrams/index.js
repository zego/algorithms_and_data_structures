// --- Directions
// Check to see if two provided strings are anagrams of eachother.
// One string is an anagram of another if it uses the same characters
// in the same quantity. Only consider characters, not spaces
// or punctuation.  Consider capital letters to be the same as lower case
// --- Examples
//   anagrams('rail safety', 'fairy tales') --> True
//   anagrams('RAIL! SAFETY!', 'fairy tales') --> True
//   anagrams('Hi there', 'Bye there') --> False

function anagrams(stringA, stringB) {
    stringA = stringA.replace(/[^\w]/g, "").toLowerCase();
    stringB = stringB.replace(/[^\w]/g, "").toLowerCase();

    const stringAMap = {};
    stringA.split('').map((character) => {
        stringAMap.hasOwnProperty(character) ? stringAMap[character]++ : stringAMap[character] = 1;
    })

    const stringBMap = {};
    stringB.split('').map((character) => {
        stringBMap.hasOwnProperty(character) ? stringBMap[character]++ : stringBMap[character] = 1;
    })

    if (Object.keys(stringAMap).length  !== Object.keys(stringBMap).length) return false;

    for (let key in stringAMap) {
        if (!stringBMap.hasOwnProperty(key)) return false;
        if (stringAMap[key] !== stringBMap[key]) return false;
    }

    return true;
}

module.exports = anagrams;

// Solution 2
// function anagrams(stringA, stringB) {
//     const aCharMap = bulidCharMap(stringA);
//     const bCharMap = bulidCharMap(stringB);

//     if (Object.keys(aCharMap).length !== Object.keys(bCharMap).length) return false;

//     for (let char in stringAMap) {
//         if (aCharMap[char] !== bCharMap[char]) return false;
//     }

//     return true;
// }

// function bulidCharMap(str) {
//     const charMap = {};

//     for (let char of str.replace(/[^\w]/g, "").toLowerCase()) {
//         charMap[char] = charMap[char] + 1 || 1;
//     }

//     return charMap
// }
