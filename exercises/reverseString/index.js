// --- Directions
// Given a string, return a new string with the reversed
// order of characters
// --- Examples
//   reverse('apple') === 'elppa'
//   reverse('hello') === 'olleh'
//   reverse('Greetings!') === '!sgniteerG'

// SOLUTION 1
function reverse(str) {
    let reversedString = '';

    for (let i = str.length - 1; i >= 0; i--) {
        reversedString += str[i];
    }

    return reversedString;

}

// SOLUTION 2
// function reverse(str) {

//     return str.split('').reverse().join('');

// }

// SOLUTION 3
// function reverse(str) {

//     str.split('').reduce((reversed, character) => {
//         return character + reversed 
//     }, '')

// }

module.exports = reverse;
