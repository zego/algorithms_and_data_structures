const reverse = require('./index');

test('Reverse function exists', () => {
    expect(reverse).toBeDefined();
});

test('Reverse reverses string: abcd', () => {
    expect(reverse('abcd')).toEqual('dcba')
})

test('Reverse reverses string:   abcd', () => {
    expect(reverse('  abcd')).toEqual('dcba  ')
})

test('Reverse reverses string: apple', () => {
    expect(reverse('apple')).toEqual('elppa')
})

test('Reverse reverses string: hello', () => {
    expect(reverse('hello')).toEqual('olleh')
})

test('Reverse reverses string: Greetings!', () => {
    expect(reverse('Greetings!')).toEqual('!sgniteerG')
})
