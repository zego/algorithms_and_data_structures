// --- Directions
// Given a string, return the character that is most
// commonly used in the string.
// --- Examples
// maxChar("abcccccccd") === "c"
// maxChar("apple 1231111") === "1"

function maxChar(str) {
    let aux = {}
    str.split('').map((character) => {
        aux.hasOwnProperty(character) ? aux[character] += 1 : aux[character] = 1;
    })

    let biggest = ['', 0]
    Object.entries(aux).map((value) => {
        value[1] > biggest[1] ? biggest = value : 1;
    })

    return biggest[0]
}

module.exports = maxChar

// SOLUTION 2
// function maxChar(str) {
//     const charMap = {}
//     let max = 0;
//     let maxChar = "";

//     for (let char of str) {
//         if (charMap[char]) {
//             charMap[char]++;
//         } else {
//             charMap[char] = 1;
//         }
//     }

//     for (let char in charMap) {
//         if (charMap[char] > max) {
//             max = charMap[char];
//             maxChar = char;
//         }
//     }

//     return maxChar;
// }
